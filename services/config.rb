coreo_agent_selector_rule 'check-postgres' do
    action :define
    timeout 30
    control 'check-postgres' do
        describe command('postgres') do
            it { should exist }
        end
    end
end

coreo_agent_audit_profile 'postgres-baseline' do
    action :define
    selectors ['check-postgres']
    profile 'https://github.com/dev-sec/postgres-baseline/archive/master.zip'
    timeout 120
end

coreo_agent_rule_runner 'audit-postgres-profiles' do
    action :run
    profiles ${AUDIT_POSTGRES_PROFILES_ALERT_LIST}
    filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end
  