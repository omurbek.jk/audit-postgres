Audit PostgreSQL Applicaiton
============================
This stack will perform baseline for PostgresSQL application

## Description
This composite will audit PostgresSQL application for PostgreSQL baseline

* ![DevSec PostgreSQL Baseline](https://github.com/dev-sec/postgres-baseline "Inspec profile github link")

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_POSTGRES_PROFILES_ALERT_LIST`:
  * description: 
  * default: postgres-baseline


## Optional variables with no default

### `FILTERED_OBJECTS`:
  * description: JSON object of string or regex of aws objects to include or exclude and tag in audit

## Tags
1. Audit
1. Best Practices
1. CIS

## Categories


## Diagram


## Icon


